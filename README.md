# [SPIKE] - Server side events (SSE)

### Description

This is a very basic sample of SSE interaction between server and client
[see React JS implementation here](https://bitbucket.org/kristian-lonergan/sse-reactjs-spike/src/master/)

## Prerequisites

- Have [NodeJS](https://nodejs.org/en/download/) installed

## Installation

Just clone/download the package and that's it.

### How to run

In the folder of the project run: `npm run start`.

Open the client.html file in your favourite browser (you don't need a web server).

For example, if you are using chrome, open the dev tools and go to the console drawer
and you should see console logs of events (an event is sent every 2 seconds - this is configurable in the server.js)

### Extra info

Server side code handled in server.js

Client side code handled in a script tag in client.html file
